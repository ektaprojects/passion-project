﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Playlist
    {
        [Key]
        public int PlayListID { get; set; }

        [Required, StringLength(200), Display(Name ="Playlist")]
        public string PlayListName { get; set; }

        //one(Playlist) to many(Songs) relation
        public virtual ICollection<Song> Songs { get; set; }

    }
}