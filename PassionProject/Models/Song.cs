﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace PassionProject.Models
{
    public class Song
    {
        [Key]
        public int SongID { get; set; }

        [Required, StringLength(200), Display(Name = "Song")]
        public string SongName { get; set; }

        [Required, StringLength(400), Display(Name = "Song Description")]
        public string SongDescription { get; set; }

        //one playlist has many songs
        public Playlist playlist { get; set; }
    }
}