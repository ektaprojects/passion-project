﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PassionProject.Models.ViewModels
{
    public class SongEdit
    {
        //Empty constructor
        public SongEdit()
        {

        }
        
        public virtual Song song { get; set; }

        public IEnumerable<Playlist> playlists { get; set; }

    }
}