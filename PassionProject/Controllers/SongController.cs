﻿using PassionProject.Models;
using PassionProject.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;

// Reference from Christine's class example
namespace PassionProject.Controllers
{
    public class SongController : Controller
    {
        private PlaylistCMSContext db = new PlaylistCMSContext();
        // GET: Song
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult New()
        {
            //New.cshtml asks for information about all playlists
            return View(db.Playlists.ToList());
        }
        public ActionResult List()
        {
            IEnumerable<Song> songs = db.Songs.ToList();
            return View(songs);
        }
        [HttpPost]
        public ActionResult Create(String SongName_New, String SongDescription_New, int? PlayList_New)
        {
            string query = "insert into Songs (SongName, SongDescription, playlist_PlayListID) " +
                "values(@name,@description,@pid)";
            SqlParameter[] mysqlparams = new SqlParameter[3];
            mysqlparams[0] = new SqlParameter("@name", SongName_New);
            mysqlparams[1] = new SqlParameter("@description", SongDescription_New);
            mysqlparams[2] = new SqlParameter("@pid", PlayList_New);

            db.Database.ExecuteSqlCommand(query, mysqlparams);
            Debug.WriteLine(query);

            return RedirectToAction("List");
        }

        public ActionResult Show(int id)
        {
            string query = "select * from songs where songid = @id";

            return View(db.Songs.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }

        public ActionResult Edit(int id)
        {
            SongEdit songeditview = new SongEdit();
            songeditview.song = db.Songs.Find(id);
            songeditview.playlists = db.Playlists.ToList();

            return View(songeditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string SongName, string SongDescription, int? SongPlaylist)
        {
            if ((id == null) || (db.Songs.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update songs set SongName=@songname," +
                "SongDescription=@songdescription," +
                "playlist_PlayListID = @pid where SongID=@id";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@songname", SongName);
            myparams[1] = new SqlParameter("@songdescription", SongDescription);
            myparams[2] = new SqlParameter("@pid", SongPlaylist);
            myparams[3] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Songs.Find(id) == null))
            {
                return HttpNotFound();

            }

            //delete songs
            string query = "delete from Songs where songid=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            return RedirectToAction("List");
        }

    }
}