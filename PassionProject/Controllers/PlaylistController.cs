﻿using PassionProject.Models;
using PassionProject.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
// Reference from Christine's class example
namespace PassionProject.Controllers
{
    public class PlaylistController : Controller
    {
        private PlaylistCMSContext db = new PlaylistCMSContext();
        // GET: Playlist
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }
        public ActionResult New()
        {

            return View();
        }
        public ActionResult List()
        {
            IEnumerable<Playlist> playlists = db.Playlists.ToList();
            return View(playlists);
        }

        [HttpPost]
        public ActionResult New(String Playlist_New, Playlist playlist)
        {
            string query = "insert into Playlists (PlayListName) values(@name)";
            SqlParameter[] mysqlparams = new SqlParameter[1];
            mysqlparams[0] = new SqlParameter("@name", Playlist_New);

            db.Database.ExecuteSqlCommand(query, mysqlparams);
            Debug.WriteLine(query);

            return RedirectToAction("List");
        }

        public ActionResult Edit(int id)
        {
            PlaylistEdit playlisteditview = new PlaylistEdit();
            playlisteditview.playlist = db.Playlists.Find(id);

            return View(playlisteditview);
        }

        [HttpPost]
        public ActionResult Edit(int id, string PlayListName)
        {
            if ((id == null) || (db.Playlists.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "update Playlists set PlayListName=@playlistname where PlayListID=@id";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@playlistname", PlayListName);
            myparams[1] = new SqlParameter("@id", id);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("Show/" + id);
        }

        public ActionResult Show(int id)
        {
            if ((id == null) || (db.Playlists.Find(id) == null))
            {
                return HttpNotFound();

            }
            string query = "select * from Playlists where PlayListID = @id";

            return View(db.Playlists.SqlQuery(query, new SqlParameter("@id", id)).FirstOrDefault());
        }


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Playlists.Find(id) == null))
            {
                return HttpNotFound();

            }

            //delete associated songs
            string query = "delete from Songs where playlist_PlaylistID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            //delete playlist
            query = "delete from Playlists where PlayListID=@id";
            param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");
        }

    }
}